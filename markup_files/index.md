![Bild 1](media/1.jpeg)

**Utrymmen tas fram som en del av projektets informationsmodeller. I produktionsfasen ska utrymmets specifikationer förverkligas med avgränsande väggar, öppningar för fönster och dörrar samt utrustning.**

# Information om utrymmen förs vidare med hjälp av BIM

> ##### En obruten informationskedja under en byggnads hela livscykel kan skapas genom att utnyttja de öppna standarder som idag finns tillgängliga. Information i tidiga skeden kan redan hanteras i formatet fi2XML i kombination med IFC . Det visar OpenBIM-projektet The Life of a Space som nyligen genomförts.

PROJEKTETS SYFTE VAR ATT PRAKTISKT BESKRIVA och visa hur grundläggande information om ett utrymme kan följa med, användas, kompletteras och förändras under ett byggnadsverks hela livscykel, framför allt gällande informationsbehovet
i drift- och förvaltningsskedet. 
​	För åtskilliga aktörer i dagens bygg- och förvaltningsprocesser är denna information inte tillgänglig och det saknas
gemensamma definitioner och standarder. Det saknas även medvetenhet om möjligheterna att kunna använda informationen redan i tidiga skeden för att förbereda den kommande driften och förvaltningen av byggnaderna.
​	– Vi har definierat och visat att information i tidiga skeden redan kan hanteras i formatet fi2XML i kombination med IFC, säger Väino Tarandi, professor i byggandets informationsteknologi, KTH, som tillsammans med Anders Moberg, vice vd Affärsområdeschef Cad-Q Fastighet, utgjort projektledning. Vi har utgått från rummet som ett centralt objekt i byggande och förvaltning och fyllt på med information om areor, golvmaterial och städbarhet.
​	Projektledningen har tillsammans med en arbetsgrupp, som bestått av representanter för tre landstingsfastighetsbolag, utgått från den nyligen byggda akut- och infektionsmottagningen vid Skånes universitetssjukhus i Malmö där Revitmodeller
använts. Programvaran dRofus har använts för att beskriva ett utrymmes krav och vilken information och utrustning som är kopplad till rummen. 
​	Ett utrymmes ursprung kommer från ett verksamhetsbehov och specificeras i tidigt skede i en rumsspecifikation som ofta
kallas rumsfunktionsprogram RFP. Här skapas viktiga klassifikationer som utrymmesidentitet, funktionsbeskrivning och uppvärmningstyp.
​	Under projekteringen kompletteras utrymmet med flera egenskaper, där arean är den viktigaste för fastighetsägaren. I produktionsfasen förverkligas utrymmets specifikationer med avgränsande väggar, öppningar för fönster och dörrar samt utrustning av olika slag. Under var och en av dessa faser tillkommer mer och mer information.
​	I slutfasen tas utrymmena i bruk och nu kopplas information om hyresgäster och hyror till dessa. Utrymmena används under en tidsperiod innan det är dags för förändring, då de befintliga utrymmenas specifikationer kan användas som underlag inför en ny livscykelfas.
​	– Genom att tillämpa existerande IT-system för de olika skedena fylls utrymmet med data som baseras på tillgängliga klassificeringsstandarder som fi2, IFC och BSAB. Vårt projekt visar hur det praktiskt kan genomföras och ligga till grund för framtida kravställningar som ska leda till effektiviseringar i arbetsprocesser och minimera omregistrering av data som redan skapats i tidigare skeden.
​	Det är viktigt med obrutna informationskedjor så att det inte blir några fel. Ju mer information desto viktigare att inte manuellt plocka ut och föra in den i nya system. Obrutna flöden gör processen säkrare och effektivare. En obruten informationskedja kan skapas med definierade informationsleveranser som skapas av en aktör och lagras i databaser och/eller tas emot av en annan aktör i processen.
​	– I vårt exempel har vi bara visat vad fastighetsförvaltaren vill ha. Den informationen ska utökas med den som arkitekter, konstruktörer och entreprenörer vill ha knuten till de här rummen, vilket kommer att ställa krav på en öppen och standardiserad information så att man lätt kan knyta olika verktyg till denna. Man ska kunna tanka in information i till exempel inköps- och planeringsverktyg, säger Väino Tarandi och fortsätter:
​	– Det stora arbetet handlar inte om tekniken och formaten utan om att komma överens om alla begrepp. Fi2 och IFC har datastrukturer som talar om hur objekt ska överföras och hur egenskaper ska formateras men deras definitioner och namn
är svårare att hantera. Vissa egenskaper finns inte ens i processerna idag.

BASALA DELAR SOM ATT HÅLLA REDA PÅ area, funktion, användning och lite till kan hanteras idag men det återstår ett stort standardiseringsarbete för att kunna hantera andra egenskaper som till exempel olika typer av temperaturer, fuktighet och
energibelastningar från utrustning. Inga verktyg kan förstå dessa egenskaper och lägga in dem i en fortsatt hantering i beräkningsprogram om det saknas branschgemensamma överenskommelser.
​	En byggherre/beställare som vill arbeta med BIM i förvaltningsskedet bör anlita någon som kan hjälpa till att komma igång. Man måste också kontrollera med programvaruleverantören om programvaran kan användas för exporter av fi2XML eller IFC. Det är inte alla programvaror som i dag har dessa inbyggda.

DEN STORA NYTTAN MED BIM I FÖRVALTNINGSSKEDET och att kunna tanka in information så tidigt som möjligt är att man tidigt får
grepp om storleken på ytorna och kan göra kostnadsbedömningar,
inte direkt för produktionen utan mer för att förbereda
kommande drift. Beroende på vilken form av projekt det
handlar om kan man börja planera marknadsföring gentemot
kunder och visa vilka rumstyper och kvadratmeter man kan
erbjuda.
​	Man kan även tidigt – redan utifrån bygglovshandlingen – knyta planerade intäkter till rummen och börja handla upp drift, till exempel städning och energi. Genom att koppla tillverkarnas skötselanvisningar till rummen kan man se vad
dessa innebär för driften. Allt sammantaget ger tidigt en bra precision i planeringen.
​	– Tack vare BIM kan vi få en helt annan framförhållning. Vi har allt att vinna på tidig information, säger Väino Tarandi.
​	Det som återstår att göra är att förankra alla de egenskaper som ska finnas med utifrån namn, funktion och värden. De svenska landstingen har olika kodtabeller och här försöker FFI, Föreningen för förvaltningsinformation, få till stånd en standard. Dessutom måste programvaruföretagen komplettera sina program.

DRYGT ETT FEMTONTAL FÖRETAG HAR MEDVERKAT i projektet och deltagit i de seminarier som anordnats.
​	– Vi vill att några av de medverkande företagen genomför ett antal pilotprojekt så att vi får erfarenhet av detta arbetssätt. Än så länge finns inga exempel på där man i begränsad omfattning använt det fullt ut, så det behöver testas. Dessutom måste någon i programvarubranschen ta tag i frågan och användarna måste ställa krav. Först då blir BIM inom det här området en realitet.
​	Väino Tarandi och hans kollegor bygger på KTH upp ett laboratorium för att kunna läsa in information om byggnaden och tanka ut leveransspecifikationer. Här kommer forskning och workshops att äga rum. 